<?php session_start();
if(!isset($_SESSION['panier'])){
    header('location:produit.php');
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="panier.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
    <title>Panier</title>
</head>

<body>
    <header>
        <div class="wrapper">
            <a href="index.php">
                <img class="logo" src="./images/FERMEcrop.png" alt="logo" width="250px" center="" cover="">
            </a>
        </div>
        <div class="wrapper" id="title">
            <h1 class="title">La Ferme A.T.O.G - Click & Collect</h1>
        </div>

    </header>

    <nav>
        <ul>
            <li><a href="produit.php">Produit</a></li>
            <li><a href="team.php">Equipe</a></li>
            <li><a href="espace_connexion.php">Connexion</a></li>
        </ul>
    </nav>


    <div class="monpanier">
        <h1 class=monpanierh2><span>MON PANIER</span></a></h1>
    </div>

    <?php
    include "../model/data.php";
    // include "../view/debug.php";
    $panier = $_SESSION['panier'];
    $total = 0;
    $kg = 0;
    foreach ($panier as $produit) {
        $id = $produit['id'];
        $quantite = $produit['quantite'];
        $select = getProduitById($id)[0];
        $prixUnProduit = 0;
        $prixUnProduit = $quantite * $select['prixAuKg'];
        $total += $prixUnProduit; 
        $kg += $quantite;
    ?>


        <div class="container mt-5 mb-5 ">
            <div class="d-flex justify-content-center row">
                <div class="col-md-10">
                    <div class="row p-2 bg-white border rounded">
                        <div class="col-md-3 mt-1"><img class="image_produit" src="./images/upload/<?php echo $select['image_produit'] ?>"></div>
                        <div class="col-md-6 mt-5 text-center ">
                            <h5 class="nom"><?php echo $select['nom'] ?></h5>

                        </div>
                        <div class="align-items-center align-content-center col-md-3 border-left mt-1">
                            <div class=" flex-row align-items-center">
                                <h4 class="prixAuKg">Prix:<?php echo $prixUnProduit ?>€</h4>
                                <h4 class="prixAuKg">Qt prise  :<?php echo $quantite ?> Kg</h4>
                                <!-- <form action="../controller/validePanier.php" method="post">
                    <label class="quantite_disp" for="tentacles">Quantité/kg:</label>

                    <input class="inputqt" id='1' data-prix="<?php echo $select['prixAuKg']; ?>" data-kg="0.500" name="2" value="<?php echo $quantite ?>" min="0" type="number" step='0.500'>  -->

                                <!-- </form> -->


                                
                                   
                                


                            </div>


                        </div>

                    </div>
                </div>

                

            <?php  } 
            
            
            ?>
            <div class ="global">
               <div class = "panierPrix">
                    <p>Total en Kg: <span id="total"><?php echo $kg ?></span>kg</p>
                    <p>Total : <span id="totalkg"><?php echo $total ?></span>€</p>
                </div>    
            <div class="panier">
                <h1 class=panierh1><a class="lien" href="../controller/validePanier.php" rel="index,follow">VALIDER MON PANIER</a></h1>
            </div>
    </diV>

            <script src="total.js"></script>
</body>

</html>