<?php 
            session_start();
            // include "../view/debug.php";
            include "../model/data.php";
            include 'protected.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="produit.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
    <title>Produit Admin</title>
</head>
<body>
<header>
         <div class="wrapper">
            <a href="index.php">
             <img class="logo" src="./images/FERMEcrop.png" alt="logo" width="250px" center="" cover="">
            </a>
         </div>
            <div class="wrapper" id="title">
                <h1 class = "title">La Ferme A.T.O.G - Click & Collect</h1>
            </div>
</header>

<!--card 1-->
<div class="container mt-5 mb-5 ">
    
<form action ="../controller/insert.php" method="post" enctype="multipart/form-data">
    <div class="d-flex justify-content-center row">
        <div class="col-md-10">
            <div class="row p-2 bg-white border rounded">   
                <div class="col-md-3 mt-1">
                    <input class= "fichier" name="img" type="file" accept="image/*">
                </div>
                <div class="col-md-6 mt-5 text-center ">
                    <h5 class="nom"><input type="text" id="nom" name="nom" placeholder="Nom du produit" required ></h5>
                    
                    <h6 class="disponibilité">
                        <label for="pet-select">Disponibilités:</label>
                            <select name="dispo" id="disp-select">
                                <option value="1" boolval(1)>disponible</option>
                                <option value="0" boolval(0)>non-disponible</option>
                            </select>
                    </h6>
                </div>

                <div class="align-items-center align-content-center col-md-3 border-left mt-1">
                    <div class="d-flex flex-row align-items-center">
                        <h4 class = "prixAuKg">
                        <label class="prix_kilo" for="prix">Prix: </label>
                        <input type="number" id="prix" name="prix" min="1" max="100">/ Kg</h4>
                    </div>
                    <div class="d-flex flex-column mt-4">

                    <label class="quantite_disp" for="quantite">Dispo Stock: </label>
                    <input type="number" id="quantite" name="quantite" min="1" max="100">
                      
                    <input class="btn btn-outline-primary btn-sm mt-2"type="submit"></input>
                </div>  
            </div>
        </div>
    </div>

</form>
</div>
<?php
            foreach(getProduit() as $select){
        ?>
<div class="container mt-5 mb-5 ">
    <div class="d-flex justify-content-center row">
        <div class="col-md-10">
            <div class="row p-2 bg-white border rounded">
                <div class="col-md-3 mt-1 ">
                    <img class="image_produit" src ="images/upload/<?php echo $select['image_produit']?>">
                </div>
                <div class="col-md-6 mt-5 text-center ">
                    <h5 class="nom"><?php echo $select['nom'];?>
                    </h5>
                    <h6 class="disponibilité"><?php if($select['disponibilite'] == 1){
                        echo "disponible";
                        }else{
                        echo "non-disponible";}?>
                    </h6>
                </div>
                <div class="align-items-center align-content-center col-md-3 border-left mt-1">
                    <div class="d-flex flex-row align-items-center">
                        <h4 class="prixAuKg">Prix: <?php echo $select['prixAuKg'];?>€/ Kg</h4>
                    </div>
                    <form action="../controller/delete.php" method="post">

                        <button type="submit" name="supp" value="<?php echo $select['id']?>"><i class="fas fa-trash"></i></button>
                    </form>

                    <form action="../view/testBDD.php" method="post">
                        <button type="submit" name="modif" value="<?php echo $select['id']?>">Modifier</button>
                    </form> 
                    
                    
                </div>
                    
                    <div class="d-flex flex-column mt-4"></div>
                    <div class="quantite_disp">Stock:<?php echo $select['quantite_disp'];?> </div>
                    
            </div>
        </div>
                      
    </div>
</div>
            <?php } ?>
            <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
</body>
</html>
