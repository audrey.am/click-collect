<?php

session_start();
include '../model/data.php';
include 'protected.php';

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="espace_client_admin.css">
    
    <title>Espace Admin</title>
</head>
<body>
<header>
        <div class="wrapper">
            <a href="index.php">
            <img class="logo" src="./images/FERMEcrop.png" alt="logo" width="250px" center="" cover="">
            </a>
        </div>
            <div class="wrapper" id="title">
                <h1 class = "title">La Ferme A.T.O.G - Click & Collect</h1>
            </div>

    </header>

    <nav>
       <ul>
         <li><a href="produit_admin.php">Produit</a></li>
         <li><a class="connexionButton" href="espace_connexion.php">Connexion</a></li>
       </ul>
    </nav>

    <div class="container">
<div id="admin">

<?php foreach (getAdmin() as $admin ){?>
    <h1 class="nomAdmin">Bienvenue <span> <?php echo $admin['nom'] ?> sur l'espace Admin !!!</span></h1> <?php } ?>
    </div><!-- admin -->
</div><!-- /.container -->

<a href="Liste_Commande_admin.php"><div class="container2">
<h2 class="admin">Liste des commandes</h2>
</div></a>

<a href="../controller/sessiondelete.php"><div class="container3">
<h2 class="admin">Déconnexion</h2>
</div></a>

<!-- <a href="#"><div class="container4">
<h2 class="admin">Supprimer un profil admin/client</h2> ===========fonction pas mise en place=========
</div></a> -->

</body>
</html>