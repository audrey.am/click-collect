
<?php include 'header.php' ;
include '../model/data.php';
?>

    <nav>
       <ul>
         <li><a href="produit.php">Produit</a></li>
         <li><a href="team.php">Equipe</a></li>
         <li><a href="espace_connexion.php">Connexion</a></li>
       </ul>
    </nav>

  
    <div class="container">

<div id="Thestory" class="clearfix">
    <h1 class = "rouge">Histoire</h1>

    <?php foreach (recupInformation()as $info) { ?>
    <p class="lead"><?php echo $info['histoire1'] ?>!</p>
    <div id="story-excerpt">
        <p><?php echo $info['histoire2'] ?></p>
    </div><!-- /#story-excerpt -->
    <div id="best-market">
        <h3>Meilleure Ferme de France</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur.</p>
    </div><!-- /#best-market -->
    <div class="btn">
  <a class="hvr-float-shadow" href="https://www.facebook.com/sharer/sharer.php?u=http://www.jasonleewilson.com/" target="_blank" title="Share on Facebook"><i class="fa fa-facebook"></i></a>

  <a class="hvr-float-shadow" href="https://twitter.com/intent/tweet?source=http://www.jasonleewilson.com/&text=I'm supercool! I know... https://www.jasonleewilson.com/&via=jasonleewilson" target="_blank" title="Tweet"><i class="fa fa-twitter fa-lg"></i></a>

  <a class="hvr-float-shadow" href="https://plus.google.com/share?url=http://www.jasonleewilson.com/" target="_blank" title="Share on Google+"><i class="fa fa-linkedin"></i></a>

  <a class="hvr-float-shadow" href="https://pinterest.com/pin/create/button/?url=http://www.jasonleewilson.com/&media=http://www.jasonleewilson.com/wp-assets/uploads/2013/03/social-tw-jasonleewilson.jpg&description=I'm supercool! I know..." target="_blank" title="Pin it"><i class="fa fa-pinterest"></i></a>

</div>
  
    <?php } ?>
</div><!-- /#whole-story -->

<hr>


</div><!-- /.container -->

<footer> 

<div id='main'>
   <?php foreach(recupInformation() as $info) { ?>
      
  <article>
      <h3>Adresse</h3>
      <p class = "adresse"><?php echo $info['adresse'] ?></p>
      
  </article>

  <nav>
      <h3>Téléphone</h3>
      <p class = "tel"><?php echo $info['telephone'] ?></p>
  </nav>

  <aside>
      <h3>Horaires d’ouverture/
          fermeture</h3>
      <p class = "horaire"><?php echo $info['horaire'] ?></p>
      
  </aside>
  
  <?php } ?>
  </div> 
  
</footer>

<script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
 </body>
</html>