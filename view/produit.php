
            <?php session_start(); 
            // include "../view/debug.php";
            include "../model/data.php";
            ?>
            

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="produit.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
    <title>Produit</title>
</head>
<body>
<header>
        <div class="wrapper">
            <a href="index.php">
            <img class="logo" src="./images/FERMEcrop.png" alt="logo" width="250px" center="" cover="">
            </a>
        </div>

        <div class="wrapper" id="title">
            <h1 class = "title">La Ferme A.T.O.G - Click & Collect</h1>
        </div>

    </header>

    <nav>
       <ul>
         <li><a href="produit.php">Produit</a></li>
         <li><a href="team.php">Equipe</a></li>
         <li><a href="espace_connexion.php">Connexion</a></li>
       </ul>
    </nav>

<!--card 1-->

<i onclick="myFunction()" class="fas fa-eye"></i>

<?php
            foreach(getProduit() as $select){
        ?>
        
<div class=" container mt-5 mb-5 ">
    <div class="d-flex justify-content-center row">
        <div class="col-md-10">
            <div class=" shadow p-1 row p-2 bg-white border rounded">
                <div class="col-md-3 mt-1 ">
                    <img class="image_produit" src ="images/upload/<?php echo $select['image_produit']?>">
                </div>
                <div class="col-md-6 mt-5 text-center ">
                    <h5 class="nom" ><?php echo $select['nom'];?>
                    </h5>
                    <h6 class="disponibilité"><?php if($select['disponibilite'] == 1){
                        echo "disponible";
                        }else{
                        echo "non-disponible";}?>
                    </h6>
                </div>
                <div class="align-items-center align-content-center col-md-3 border-left mt-3">
                    <div class="d-flex flex-row align-items-center mt-6 mb-6 ">
                                 
                        <h4 id ="prixaukg" class="prixAuKg" value = "<?php echo $select['prixAuKg'];?>">Prix: <?php echo $select['prixAuKg'];?>€/ Kg</h4>
                       
                    </div>
                    <div class="d-flex flex-column mt-4">
                   
                    <form action="../controller/ajoutPanier.php" method="post">
                        <label class="quantite_disp" for="tentacles"  value="<?php echo $select['quantite_disp']?>">Quantité/kg:</label>
                        <input class="inputqt" id='1' data-prix="<?php echo $select['prixAuKg'];?>" data-kg="0.500" name="2" value="0" min="0" type="number" step='0.500' required> 
                        <button class="btn btn-outline-primary btn-sm mt-2" type="submit" name="nom" value="<?php echo $select['id'];?>">Ajouter au Panier</button>
                    </form> 
                       
                        
                        </div>
                    
               
                </div>
            </div>
                    
                </div>
                    
                    
                    
            </div>
        </div>
                      

            <?php } ?>

            
            <div class ="global">
                <div class = "panierPrix">
                    <p>Total : <span id="total">0</span>€</p>
                    <p>Total : <span id="totalkg">0</span>Kg</p>
                </div>    
                 <div class= "panier">
                    <h1 class=panierh1><a class="lien" href="panier.php" rel="index,follow">VOIR MON PANIER</a></h1>
                 </div> 

                 <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
<script src="total.js"></script>
</body>
</html>

