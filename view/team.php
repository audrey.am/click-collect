<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="team.css">
    <title>Notre équipe</title>
</head>
<body>
<header>
        <div class="wrapper">
        <a href="index.php">
            <img class="logo" src="./images/FERMEcrop.png" alt="logo" width="250px" center="" cover="">
</a>
        </div>
        <div class="wrapper" id="title">
            <h1 class = "title">La Ferme A.T.O.G - Click & Collect</h1>
        </div>

    </header>

    <nav>
       <ul>
         <li><a href="produit.php">Produit</a></li>
         <li><a href="team.php">Equipe</a></li>
         <li><a href="espace_connexion.php">Connexion</a></li>
       </ul>
    </nav>
        
       
      <div class="wrapper">
        <h1>Notre équipe</h1>
        <div class="team">
          <!-- CARD DE GUILLAUME -->
          <div class="team_member">
            <div class="team_img">
              <img src="https://qse.uae.ma/wp-content/uploads/2018/07/account_avatar_male_man_person_profile_user_icon_434194.png" alt="Team_image">
            </div>
            <h3>Guillaume</h3>
            <p class="role">Product Owner / Développeur</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Est quaerat tempora, voluptatum quas facere dolorum aut cumque nihil nulla harum nemo distinctio quam blanditiis dignissimos.</p>
            <ul class="social">
                        <li><a href="https://gitlab.com/" class="fa fa-gitlab"></a></li>
                        <li><a href="https://codepen.io/" class="fa fa-codepen"></a></li>
                        <li><a href="https://fr.linkedin.com/" class="fa fa-linkedin"></a></li>
                    </ul>
          </div>
          <!-- END CARD DE GUILLAUME  -->

          <!-- CARD DE AUDREY -->
          <div class="team_member">
            <div class="team_img">
              <img src="https://qse.uae.ma/wp-content/uploads/2018/07/account_avatar_male_man_person_profile_user_icon_434194.png" alt="Team_image">
            </div>
            <h3>Audrey</h3>
            <p class="role">Développeuse</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Est quaerat tempora, voluptatum quas facere dolorum aut cumque nihil nulla harum nemo distinctio quam blanditiis dignissimos.</p>
            <ul class="social">
                        <li><a href="https://gitlab.com/" class="fa fa-gitlab"></a></li>
                        <li><a href="https://codepen.io/" class="fa fa-codepen"></a></li>
                        <li><a href="https://fr.linkedin.com/" class="fa fa-linkedin"></a></li>
                    </ul>
          </div>
          <!-- END CARD DE AUDREY -->

          <!-- CARD DE TIZIO -->
          <div class="team_member">
            <div class="team_img">
              <img src="https://qse.uae.ma/wp-content/uploads/2018/07/account_avatar_male_man_person_profile_user_icon_434194.png" alt="Team_image">
            </div>
            <h3>Tizio</h3>
            <p class="role">Developpeur Back-End</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Est quaerat tempora, voluptatum quas facere dolorum aut cumque nihil nulla harum nemo distinctio quam blanditiis dignissimos.</p>
            <ul class="social">
                        <li><a href="https://gitlab.com/" class="fa fa-gitlab"></a></li>
                        <li><a href="https://codepen.io/" class="fa fa-codepen"></a></li>
                        <li><a href="https://fr.linkedin.com/" class="fa fa-linkedin"></a></li>
                    </ul>
          </div>
         <!-- END CARD DE TIZIO -->
         
         <!-- CARD DE OVSEP -->
          <div class="team_member">
            <div class="team_img">
              <img src="https://qse.uae.ma/wp-content/uploads/2018/07/account_avatar_male_man_person_profile_user_icon_434194.png" alt="Team_image">
            </div>
            <h3>Ovsep</h3>
            <p class="role">Développeur</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Est quaerat tempora, voluptatum quas facere dolorum aut cumque nihil nulla harum nemo distinctio quam blanditiis dignissimos.</p>
            <ul class="social">
                        <li><a href="https://gitlab.com/" class="fa fa-gitlab"></a></li>
                        <li><a href="https://codepen.io/" class="fa fa-codepen"></a></li>
                        <li><a href="https://fr.linkedin.com/" class="fa fa-linkedin"></a></li>
                    </ul>
          </div>
          <!-- END CARD DE OVSEP -->
        </div>
      </div>
    </div>
    </div>
    </div>
    

    <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
    <script src="team.js"></script>
</body>
</html>