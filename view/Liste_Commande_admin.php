<?php
session_start();
include '../model/data.php';
// include '../view/debug.php';
include 'protected.php'
?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="Liste_Commande_admin.css">
  <title>Liste Commande Admin</title>
</head>
<body>
<header>
         <div class="wrapper">
          <a href="index.php">
            <img class="logo" src="./images/FERMEcrop.png" alt="logo" width="250px" center="" cover="">
          </a>
        </div>
          <div class="wrapper" id="title">
            <h1 class = "title">La Ferme A.T.O.G - Click & Collect</h1>
          </div>

    </header>
    

  <div class="commande">

    <h1 class=commandeh1>Preparation de commandes</a></h1>
  </div>
  <!-- table A FAIRE -->
  <div class="commande1et2">
    <div class="table1">
      <table class="GeneratedTable">
        <thead>
          <tr>
            <th colspan="2">A FAIRE</th>
            
          </tr>
        </thead>
        <tbody>
        <?php foreach (voirCommandesAFaire() as $Cafaire) { ?>
            <tr>
            <td><a href="commande.php?id_commande=<?php echo $Cafaire['id'] ?>"><?php echo ' PANIER ';echo $Cafaire['id']; ?></a>             
            </td>
            </tr>
          
        <?php } ?>
        </tbody>
      </table>

    </div>
    
    <!-- end  table A FAIRE -->


    <!-- table PRETES -->
    <div class="table2">
      <table class="GeneratedTable2">
        <thead>
          <tr>
            <th colspan="2">PRETES</th>
            <?php foreach (voirCommandesPrete() as $CPrete) { ?>
          </tr>
        </thead>
        <tbody>

          <tr>
          <td><a href="commande.php?id_commande=<?php echo $CPrete['id'] ?>"><?php echo ' PANIER ';
              echo $CPrete['id'];
             ?></a></td>

          </tr>
        <?php } ?>
        </tbody>

      </table>

    </div>
    <!-- end table PRETES -->
  </div>


  <!--table RETIREES -->

  <div class="table3">
    <table class="GeneratedTable3">
      <thead>
        <tr>
          <th colspan="2">RETIREES</th>
          <?php foreach (voirCommandesRecaputilatif() as $Cvalidee) { ?>
        </tr>
      </thead>
      <tbody>
        <tr>
        <td><a href="commande.php?id_commande=<?php echo $Cvalidee['id'] ?>"><?php echo ' PANIER ';
              echo $Cvalidee['id_commande'];
             ?></a></td>
          <td><a href="commandesParClient.php?nom=<?php echo $Cvalidee['nom'] ?>"><?php
              echo $Cvalidee['nom'];
             
             ?></a></td>
        </tr>

      <?php } ?>
      </tbody>


</body>

</html>