<?php 
session_start();
include '../model/data.php';
// include 'debug.php';
$compte = $_SESSION['compte'];
$nom = $compte['pseudo'];

$name = getCommandeByClient($nom);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Liste_Commande_admin.css">
    <title>Commande</title>
</head>

<header>
    <div class="wrapper">
        <a href="index.php">
            <img class="logo" src="./images/FERMEcrop.png" alt="logo" width="250px" center="" cover="">
        </a>
    </div>
    <div class="wrapper" id="title">
        <h1 class="title">La Ferme A.T.O.G - Click & Collect</h1>
    </div>

</header>

<body>
    <div class="resumetoutelescommandes">
<?php foreach (getCommandeByClient($nom) as $commande) { ?>
<div class="resume">
    
    <h1> Commande #<?php echo $commande[0]['id_commande'] ?></h1>
   
    <?php  $total = 0; ?>
    <?php foreach (getOneCommande($commande) as $produit) { ?>
       <p><?php echo $produit['nomProduit']; ?> :
            <?php echo $produit['quantite']; ?></p>
        

        <?php $prixUnProduit = 0;
        $prixUnProduit = $produit['quantite'] * $produit['prixAuKg'];
        $total += $prixUnProduit;
        ?>

    <?php } ?>
    <p> pour un total de : </p>
    <?php
    echo $total;

    
    ?>
    €
    
</div>
<?php } ?>
    </div>
</body>

</html>