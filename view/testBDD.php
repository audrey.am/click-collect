<?php
// include "../view/debug.php";
include "../model/data.php"; 
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    $id = $_POST['modif'];
    if(isset($_POST['modif'])){
        $functionGetProduit = getProduitById($id);
    }
    
    foreach($functionGetProduit as $select){
?>
    <form action ="../controller/update2.php" method="post" enctype="multipart/form-data">
        <p>nom :</p>
        <input name="nom" id="nom" type="text" value="<?php echo $select['nom'];?>">
        <p>prix :</p>
        <input name="prix" id="prix" type="number" step="0.10" min="0" max="100" value="<?php echo $select ['prixAuKg']?>">
        <p>disponibilité :</p>
        <select name="dispo" id="disp-select">
            <option value="1" boolval(1)>disponible</option>
            <option value="0" boolval(0)>non-disponible</option>
        </select>
        <p>image :</p>
        <img src ="images/upload/<?php echo $select['image_produit']?>">
        <input name="img" id="img" type="file" value="<?php echo $select['image_produit']?>">
        <p>quantité au kilo disponible:</p>
        <input name="quantite" id="quantite" type="number" step="1" value="<?php echo $select['quantite_disp']?>"> 

        <button class="btn btn-outline-primary btn-sm mt-2" type="submit" name="id" value="<?php echo $select['id'];?>">Modifier Produit</button>
        
        
    </form>
    <?php } ?>


    
</body>
</html>

<!-- <input type="submit" name="supp" value="supprimer <?php echo $select['id']?>"> -->