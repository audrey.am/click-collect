<?php
include "../model/data.php";
include '../view/debug.php';

if(
    $_POST['id'] &&
    $_POST['nom'] !== "" &&
    $_POST['prix'] !== "" &&
    $_FILES['img'] &&
    $_POST['quantite'] !== "" 
     
){
    $id = $_POST['id'];
    $nom = $_POST['nom'];
    $prix = $_POST['prix'];
    $disponibilite = $_POST['dispo'];
    $quantite = $_POST['quantite'];
    $img = $_FILES['img']['name'];

    if (move_uploaded_file($_FILES['img']['tmp_name'], "../view/images/upload/$img")) {
        print "Téléchargé avec succès!";
    } else {
        print "Échec du téléchargement!";
    }
    

    updateProduit($nom, $prix, $img, $disponibilite, $quantite, $id);

   

    // header('location:../view/canard.php');
}



header('location:../view/produit_admin.php');
