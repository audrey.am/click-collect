DROP DATABASE IF EXISTS ClickAndCollect;
CREATE DATABASE ClickAndCollect;

USE ClickAndCollect;
GRANT ALL PRIVILEGES ON ClickAndCollect.* TO 'admin'@'localhost';

CREATE TABLE EtatCommande(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    validee BOOLEAN ,
    prete BOOLEAN,
    collectee BOOLEAN
);



CREATE TABLE CoordonneesClient(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(60),
    email VARCHAR(255),
    telephone INT,
    mot_de_passe VARCHAR(60)
);

CREATE TABLE Produit (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(60),
    image_produit VARCHAR (255),
    prixAuKg FLOAT,
    disponibilite BOOLEAN,
    quantite_disp FLOAT
);

CREATE TABLE Information (
    adresse VARCHAR (255),
    telephone INT,
    horaire VARCHAR (255),
    histoire1 VARCHAR (640),
    histoire2 VARCHAR (640)
 );

CREATE TABLE Login_admin (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR (60),
    mot_de_passe VARCHAR (60),
    role_admin BOOLEAN
);

CREATE TABLE Etat_Login (
    id_commande INT UNSIGNED,
    id_admin INT UNSIGNED,
    UNIQUE (id_commande,id_admin),
    FOREIGN KEY (id_commande) REFERENCES EtatCommande(id),
    FOREIGN KEY (id_admin) REFERENCES Login_admin(id)
);

CREATE TABLE Commande_Client(
    id_commande INT UNSIGNED,
    id_client INT UNSIGNED,
    UNIQUE (id_commande, id_client),
    FOREIGN KEY (id_commande) REFERENCES EtatCommande(id),
    FOREIGN KEY (id_client) REFERENCES CoordonneesClient(id)
);

CREATE TABLE LigneCommande (
    id_commande INT UNSIGNED,
    id_produit INT UNSIGNED,
    quantite FLOAT,
    UNIQUE (id_commande, id_produit),
    FOREIGN KEY (id_commande) REFERENCES EtatCommande(id),
    FOREIGN KEY (id_produit) REFERENCES Produit(id)
);

INSERT INTO Login_admin (nom, mot_de_passe, role_admin) VALUES ("Joe", "bigjoe1234", 1);
INSERT INTO CoordonneesClient(nom, email, telephone, mot_de_passe) VALUES ("Kelly", "kelly@gmail.com", 0606060606, "kelly123");
INSERT INTO Information (adresse, telephone, horaire, histoire1, histoire2) VALUES ("5 rue de la Pomme, 64000 Biarritz", "0505050505", "Lundi-Samedi : 8h-16h", "Lorem ipsum dolor sit amet, consectetur adipisicing elit real donut tastes like!", 
"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur aut repudiandae debitis itaque eveniet! Eius doloribus magnam ad temporibus impedit, ullam fuga ab consequatur ea, illo eligendi rerum debitis alias atque eum id. Optio veniam, ipsam velit hic, quas nisi minus illum. Optio nesciunt repudiandae, deleniti, fuga consequatur impedit! Qui.
");

