<?php
$host = 'localhost';
$db = 'ClickAndCollect';
$user = 'admin';
$pass = 'admin';

try{
$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}
catch(PDOException $e){
    echo $e->getMessage()
;}

// function getOneProject($id){
//     global $pdo;
//     try{
//     $req = $pdo->query('SELECT * from projet WHERE id = '.$id.'');
//     return $req->fetchAll();
// }catch(Exception $e){
//     // en cas d'erreur :
//      echo " Erreur ! ".$e->getMessage();
//      echo $req;
    
//   }

function getOneCommande($id_commande) {
    global $pdo;
    $req= $pdo->prepare('SELECT *, Produit.nom AS nomProduit FROM Produit INNER JOIN LigneCommande On Produit.id = LigneCommande.id_produit INNER JOIN EtatCommande ON EtatCommande.id = LigneCommande.id_commande INNER JOIN Commande_Client On LigneCommande.id_commande = Commande_Client.id_commande INNER JOIN CoordonneesClient ON Commande_Client.id_client = CoordonneesClient.id WHERE Commande_Client.id_commande = ?');
    $req->execute([$id_commande]);
    return $req->fetchAll();

};
function getCommandeByClient($nom) {
    global $pdo;
    $req= $pdo->prepare('SELECT *, Produit.nom AS nomProduit FROM Produit INNER JOIN LigneCommande On Produit.id = LigneCommande.id_produit INNER JOIN EtatCommande ON EtatCommande.id = LigneCommande.id_commande INNER JOIN Commande_Client On LigneCommande.id_commande = Commande_Client.id_commande INNER JOIN CoordonneesClient ON Commande_Client.id_client = CoordonneesClient.id WHERE CoordonneesClient.nom = ? ');
    $req->execute([$nom]);
    return $req->fetchAll();

};

// function getAllOrderWithName(){
//     global $pdo;
//     $req = $pdo->query('SELECT * FROM Produit INNER JOIN LigneCommande On Produit.id = LigneCommande.id_produit INNER JOIN EtatCommande on LigneCommande.id_commande = EtatCommande.id');
//     return $req->fetchAll();
// };

function getProduit(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM Produit');
    return $req->fetchAll();
};

function getProduitById($id){
    global $pdo; 
    $req = $pdo->prepare('SELECT * FROM Produit WHERE id= ?');
    $req->execute([$id]);
    return $req->fetchAll();
};

function getAdmin() {
    global $pdo; 
    $req = $pdo->query('SELECT * From Login_admin');
    return $req ->fetchAll();
};

// function getClient() {
//     global $pdo;
//     $req = $pdo->query('SELECT * FROM CoordonneesClient');
//     return $req->fetchAll();
// };

function selectClientIns($email){
    global $pdo;
    $req = $pdo->prepare('SELECT * From CoordonneesClient WHERE email = ?');
    $req->execute([$email]);
};


function selectClient($nom) {
    global $pdo; 
    $req = $pdo->prepare('SELECT * From CoordonneesClient WHERE nom = ? ');
    $req->execute([$nom]);
    return $req ->fetchAll();
};

function selectAdmin($nom, $mot_de_passe){
    global $pdo;
    
    $req = $pdo->prepare('SELECT * FROM Login_admin WHERE nom = ? AND mot_de_passe = ?');
    $req->execute([$nom, $mot_de_passe]);
    return $req ->fetchAll();
};

function addCoordonneesClient($nom,$email,$telephone,$mdp) {
    global $pdo;
    $req = $pdo->prepare('INSERT INTO CoordonneesClient(nom,email,telephone,mot_de_passe) VALUES(?,?,?,?)');
    $req->execute([$nom,$email,$telephone,$mdp]);
};

// function updateCoordonneesClient($telephone, $email){
//     global $pdo;
//     $req = $pdo->prepare('UPDATE CoordonneesClient SET telephone = ? , email= ? WHERE id = ?');
//     $req->execute([$telephone, $email]);
// };

// function deleteCoordonneesClient(){
//     global $pdo;
//     $req = $pdo->prepare('DELETE FROM CoordonneesClient WHERE id = ?');
//     $req->execute();
// };


function addProduit($nom,$img,$prixAukilo,$disponibilite,$quantite){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO Produit(nom, image_produit, prixAuKg, disponibilite, quantite_disp) VALUES(?,?,?,?,?)');
    $req->execute([$nom,$img,$prixAukilo,$disponibilite,$quantite]);   
};

function updateProduit($nom,$prix,$img,$disponibilite,$quantite,$id){
    global $pdo;
    $req = $pdo->prepare('UPDATE Produit
    SET nom = ?,image_produit = ?, prixAuKg = ?, disponibilite = ?,quantite_disp = ? 
    WHERE  id = ?');
    $req->execute([$nom,$img,$prix,$disponibilite,$quantite,$id]);
    
};

function deleteProduit($effacer){

    global $pdo;
    $req = $pdo->prepare('DELETE FROM Produit WHERE id = ?');
    $req->execute([$effacer]);
};

// function aFaireListeCommandes($prete){
//     global $pdo;
//     $req = $pdo->prepare('UPDATE FROM EtatCommande SET validee = true WHERE id= ?');
//     $req->execute([$prete]);
// };

// function preteListeCommandes($valider){
//     global $pdo;
//     $req = $pdo->prepare('UPDATE FROM EtatCommande SET prete = true WHERE id= ?');
//     $req->execute([$valider]);
// };

// function valideListeCommandes ($collecter){
//     global $pdo;
//     $req = $pdo->prepare('UPDATE FROM EtatCommande SET collectee = true WHERE id= ?');
//     $req->execute([$collecter]);
// };

function voirCommandesAFaire(){
    global $pdo;
    $req= $pdo ->query('SELECT * FROM EtatCommande WHERE validee = true');
    return $req->fetchAll();
};

function voirCommandesPrete(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM EtatCommande WHERE prete = true');
    return $req->fetchAll();
};

function voirCommandesRecaputilatif(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM Produit INNER JOIN LigneCommande On Produit.id = LigneCommande.id_produit INNER Join EtatCommande ON EtatCommande.id = LigneCommande.id_commande INNER JOIN Commande_Client On LigneCommande.id_commande = Commande_Client.id_commande INNER JOIN CoordonneesClient ON Commande_Client.id_client = CoordonneesClient.id WHERE collectee = true');
    return $req->fetchAll();
};

// function modifierRole($role){
//     global $pdo;
//     $req = $pdo->prepare('UPDATE Login_admin SET role_admin = ?');
//     $req->execute([$role]);
// };

// function insertEmploye($pseudo,$mdp,$role_admin){
//     global $pdo;
//     $req = $pdo->prepare('INSERT INTO Login_admin (nom, mot_de_passe, role_admin) VALUES(?,?,?)');
//     $req->execute([$pseudo,$mdp,$role_admin]);
// };

function recupInformation(){
    global $pdo;
    $req = $pdo->query ('SELECT * FROM Information');
    return $req ->fetchAll();   
};



// function modifInformation($adresse,$telephone,$horaire){
//     global $pdo;
//     $req = $pdo->prepare('UPDATE FROM Information SET adresse = ?, telephone = ?, horaire = ? ');
//     $req->execute([$adresse,$telephone,$horaire]);
// };

function ajoutPanier($id_commande,$idProduit,$quantite){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO LigneCommande(id_commande,id_produit, quantite) VALUES(?,?,?)');
    $req->execute([$id_commande,$idProduit,$quantite]);
    
};

function ajoutPanier2($bool){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO EtatCommande(validee,prete,collectee) VALUES(1,0,0)');
    $req->execute([$bool]);
    return $pdo->lastInsertId();
};

// function voirPanierById(){
//     global $pdo;
//     $req = $pdo->query('SELECT * FROM EtatCommande 
//     -- INNER JOIN Commande_Client on EtatCommande.id = Commande_Client.id_commande 
//     -- INNER JOIN CoordonneesClient on Commande_Client.id_client = CoordonneesClient.id 
//     INNER JOIN LigneCommande on LigneCommande.id_commande = EtatCommande.id 
//     INNER JOIN Produit on Produit.id = LigneCommande.id_produit');
//     return $req->fetchAll();
// };




?>
<!-- Recuperer les commandes: SELECT * FROM Produit INNER JOIN LigneCommande WHERE produit.id = LigneCommande.id_produit INNER JOIN EtatCommande WHERE LigneCommande.id_commande = EtatCommande.id ****-->
<!-- recuperer admin: Select * From Login_admin WHERE nom = $_POST && mot_de_passe = $_POST *****-->
<!-- modifier information : UPDATE CoordonneesClient telephone = "$_POST" WHERE id ="" -->
<!-- supprimer information : DELETE FROM CoordonneesClient WHERE id ="" -->

<!-- UPDATE DELETE -->
<!-- -->

<!-- function fait
{*ajouter des choses dans le produit INSERT INTO Produit(nom, image_produit, prixAuKilo, disponibilite, quantite_disp) VALUES(?,?,?,?,?)
*enlever des choses dans le produit DELETE FROM Produit WHERE id = ?
*modifier des choses le produit UPDATE  }



*function changer le role de l'Admin UPDATE Login_admin SET role_admin ='true'

mini function:
dans la liste des commandes:
-mettre en pret*, preparé*, recap*

modification coordonnées client*
supp Coordonnees Client*

changer ce qu'il y a dans l'index (information) SELECT * FROM Information
 -->


